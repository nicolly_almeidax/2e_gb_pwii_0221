import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MotosRoutingModule } from './motos-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class MotosModule { }
